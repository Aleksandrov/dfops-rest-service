package ru.resprojects.dfops.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.resprojects.dfops.config.AppConfig;
import ru.resprojects.dfops.dto.ResponseDto;
import ru.resprojects.dfops.dto.account.AccountDto;
import ru.resprojects.dfops.exception.BadResourceException;
import ru.resprojects.dfops.exception.ErrorMessage;
import ru.resprojects.dfops.model.Account;
import ru.resprojects.dfops.model.Employee;
import ru.resprojects.dfops.service.AccountService;
import ru.resprojects.dfops.service.EmployeeService;

@RestController
@RequestMapping("/account")
@Tag(name ="Расчётный счёт работника", description = "REST API для работы с сущностью 'Account'")
public class AccountController {

    private final EmployeeService employeeService;

    private final AccountService accountService;

    public AccountController(EmployeeService employeeService, AccountService accountService) {
        this.employeeService = employeeService;
        this.accountService = accountService;
    }

    @Operation(
        summary = "Вывод лицевых счетов работника",
        description = "Возвращает список лицевых счетов выбранного работника",
        tags = { "account" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = ResponseDto.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "204",
            description = "лицевые счета у выбранного работника отсутствуют",
            content = @Content(
                schema = @Schema(),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "404",
            description = "не найден работник у которого необходимо вывести список лицевых счетов",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @GetMapping(value = "/{employee_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto<Account>> getAll(
        @Parameter(description="ID работника", required = true,
            example = "5002", schema=@Schema(implementation = Long.class))
        @PathVariable("employee_id") Long employee_id,
        @Parameter(description="Параметр запроса для постраничного вывода, задает номер текущей страницы",
            example = "1", schema=@Schema(implementation = Integer.class))
        @RequestParam(value = "page", defaultValue = "1") Integer pageNo,
        @Parameter(description="Параметр запроса для постраничного вывода, задает количество элементов на страницу",
            example = AppConfig.DEFAULT_PAGE_LIMIT, schema=@Schema(implementation = Integer.class))
        @RequestParam(value = "limit", defaultValue = AppConfig.DEFAULT_PAGE_LIMIT) Integer limit)
    {
        Employee employee = employeeService.get(employee_id);
        ResponseDto<Account> response = new ResponseDto<>(accountService.getAll(employee.getId(), pageNo, limit));
        if (response.getElements().isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        }
        return ResponseEntity.ok(response);
    }

    @Operation(
        summary = "Поиск лицевого счёта",
        description = "Возвращает информацию о лицевом счёте",
        tags = { "account" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = Account.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "404",
            description = "запрашиваемый объект не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @GetMapping(value = "/get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> getById(
        @Parameter(description="ID лицевого счёта", required = true,
            example = "5002", schema=@Schema(implementation = Long.class))
        @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(accountService.get(id));
    }

    @Operation(
        summary = "Создание новой записи",
        description = "Создаёт новую запись и возвращает созданный объект",
        tags = { "account" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = Account.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "400",
            description = "некорректный запрос",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "404",
            description = "работник для которого создаётся аккаунт не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "409",
            description = "элемент уже существует в базе",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> create(
        @Parameter(description="Создаваемый объект лицевого счёта",
            required=true, schema=@Schema(implementation = AccountDto.class))
        @RequestBody AccountDto accountDto) {
        if (accountDto == null) {
            throw new BadResourceException("Request body is null");
        }
        Employee employee = employeeService.get(accountDto.getEmployeeId());
        return ResponseEntity.ok(accountService.create(employee, accountDto.getPersonalAccount()));
    }

    @Operation(
        summary = "Удаление записи",
        description = "Удаление записи из базы данных",
        tags = { "account" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция"
        ),
        @ApiResponse(
            responseCode = "404",
            description = "элемент не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> delete(
        @Parameter(description="ID лицевого счёта", required = true,
            example = "5002", schema=@Schema(implementation = Long.class))
        @PathVariable("id") Long id) {
        accountService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

}
