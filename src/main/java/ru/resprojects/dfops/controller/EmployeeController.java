package ru.resprojects.dfops.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.resprojects.dfops.config.AppConfig;
import ru.resprojects.dfops.dto.ResponseDto;
import ru.resprojects.dfops.dto.employee.EmployeeDto;
import ru.resprojects.dfops.exception.BadRequestException;
import ru.resprojects.dfops.exception.ErrorMessage;
import ru.resprojects.dfops.model.Employee;
import ru.resprojects.dfops.service.EmployeeService;

@RestController
@RequestMapping("/employee")
@Tag(name ="Работник (водитель)", description = "REST API для работы с сущностью 'Employee'")
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Operation(
        summary = "Получить список работников",
        description = "Возвращает список работников",
        tags = { "employee" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = ResponseDto.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "204",
            description = "список работников пуст",
            content = @Content(
                schema = @Schema(),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto<Employee>> getAll(
        @Parameter(description="Параметр запроса для постраничного вывода, задает номер текущей страницы",
            example = "1", schema=@Schema(implementation = Integer.class))
        @RequestParam(value = "page", defaultValue = "1") Integer pageNo,
        @Parameter(description="Параметр запроса для постраничного вывода, задает количество элементов на страницу",
            example = AppConfig.DEFAULT_PAGE_LIMIT,schema=@Schema(implementation = Integer.class))
        @RequestParam(value = "limit", defaultValue = AppConfig.DEFAULT_PAGE_LIMIT) Integer limit)
    {
        ResponseDto<Employee> response = new ResponseDto<>(employeeService.getAll(pageNo, limit));
        if (response.getElements().isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        }
        return ResponseEntity.ok(response);
    }

    @Operation(
        summary = "Поиск работника",
        description = "Возвращает информацию о работнике",
        tags = { "employee" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = Employee.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "404",
            description = "запрашиваемый объект не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @GetMapping(value = "/get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> getById(
        @Parameter(description="ID работника", required = true,
            example = "5000", schema=@Schema(implementation = Long.class))
        @PathVariable("id") Long id) {
        return ResponseEntity.ok(employeeService.get(id));
    }

    @Operation(
        summary = "Поиск работника по e-mail",
        description = "Возвращает информацию о работнике",
        tags = { "employee" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = Employee.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "400",
            description = "некорректный запрос",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "404",
            description = "запрашиваемый объект не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> getByEmail(
        @Parameter(description="Email работника", required=true,
            example = "ivanov@example.com", schema=@Schema(implementation = String.class))
        @RequestParam(value = "email") String email) {
        if (email == null || email.isBlank()) {
            throw new BadRequestException("Email is null or empty");
        }
        return ResponseEntity.ok(employeeService.getByEmail(email));
    }

    @Operation(
        summary = "Создание новой записи",
        description = "Создаёт новую запись и возвращает созданный объект",
        tags = { "employee" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = Employee.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "400",
            description = "некорректный запрос",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "409",
            description = "элемент уже существует в базе",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> create(
        @Parameter(description="Создаваемый объект",
            required=true, schema=@Schema(implementation = EmployeeDto.class))
        @RequestBody EmployeeDto employeeDto) {
        if (employeeDto == null) {
            throw new BadRequestException("Request body is null");
        }
        Employee employee = new Employee(employeeDto.getName(), employeeDto.getEmail());
        return ResponseEntity.ok(employeeService.create(employee));
    }

    @Operation(
        summary = "Удаление записи",
        description = "Удаление записи из базы данных",
        tags = { "employee" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция"
        ),
        @ApiResponse(
            responseCode = "404",
            description = "элемент не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> delete(
        @Parameter(description="ID работника", required = true,
           example = "5000", schema=@Schema(implementation = Long.class))
        @PathVariable("id") Long id) {
        employeeService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

}
