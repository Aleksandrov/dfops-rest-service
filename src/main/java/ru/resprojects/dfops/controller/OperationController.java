package ru.resprojects.dfops.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.resprojects.dfops.config.AppConfig;
import ru.resprojects.dfops.dto.ResponseDto;
import ru.resprojects.dfops.dto.account.AccountBalanceDto;
import ru.resprojects.dfops.dto.operation.OperationAmountDto;
import ru.resprojects.dfops.dto.operation.OperationDto;
import ru.resprojects.dfops.dto.operation.OperationTransferDto;
import ru.resprojects.dfops.exception.BadRequestException;
import ru.resprojects.dfops.exception.BadResourceException;
import ru.resprojects.dfops.exception.ErrorMessage;
import ru.resprojects.dfops.model.Account;
import ru.resprojects.dfops.service.AccountService;
import ru.resprojects.dfops.service.OperationService;
import ru.resprojects.dfops.util.DateTimeUtil;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/operation")
@Tag(name ="Операции над счётом", description = "REST API для работы с сущностью 'Operation'")
public class OperationController {

    private final AccountService accountService;

    private final OperationService operationService;

    public OperationController(AccountService accountService, OperationService operationService) {
        this.accountService = accountService;
        this.operationService = operationService;
    }

    @Operation(
        summary = "Вывод всех операций по лицевому счёту работника",
        description = "Возвращает список всех операций по выбранному лицевому счету работника",
        tags = { "operation" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = ResponseDto.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "204",
            description = "список операций по счёту пуст",
            content = @Content(
                schema = @Schema(),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "404",
            description = "лицевой счёт не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @GetMapping(value = "/{account_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto<ru.resprojects.dfops.model.Operation>> getAll(
        @Parameter(description="ID лицевого счёта", schema=@Schema(implementation = Long.class), required = true, example = "5002")
        @PathVariable("account_id") Long accountId,
        @Parameter(description="Параметр запроса для постраничного вывода. Задает номер текущей страницы",
            example = "1", schema=@Schema(implementation = Integer.class))
        @RequestParam(value = "page", defaultValue = "1") Integer pageNo,
        @Parameter(description="Параметр запроса для постраничного вывода. Задает количество элементов на страницу",
            example = AppConfig.DEFAULT_PAGE_LIMIT, schema=@Schema(implementation = Integer.class))
        @RequestParam(value = "limit", defaultValue = AppConfig.DEFAULT_PAGE_LIMIT) Integer limit)
    {
        Account account = accountService.get(accountId);
        ResponseDto<ru.resprojects.dfops.model.Operation> response = new ResponseDto<>(operationService.getAllByAccountId(account.getId(), pageNo, limit));
        if (response.getElements().isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        }
        return ResponseEntity.ok(response);
    }

    @Operation(
        summary = "Вывод всех операций за период по лицевому счёту работника",
        description = "Возвращает список всех операций за период по выбранному лицевому счету работника",
        tags = { "operation" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = ResponseDto.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "204",
            description = "список операций по счёту пуст",
            content = @Content(
                schema = @Schema(),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "400",
            description = "некорректный запрос",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "404",
            description = "лицевой счёт не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @GetMapping(value = "/{account_id}/filter", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto<ru.resprojects.dfops.model.Operation>> getAllBetween(
        @Parameter(description="ID лицевого счёта", schema=@Schema(implementation = Long.class), required = true, example = "5002")
        @PathVariable("account_id") Long accountId,
        @Parameter(description="Начальная дата и время (формат dd.MM.yyyy-HH:mm)", schema=@Schema(implementation = String.class),
            example = "01.05.2020-00:00", required = true)
        @RequestParam(value = "startDateTime") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_PATTERN_REQUEST) LocalDateTime startDateTime,
        @Parameter(description="Конечная дата и время (формат dd.MM.yyyy-HH:mm)", schema=@Schema(implementation = String.class),
            example = "31.05.2020-23:59", required = true)
        @RequestParam(value = "endDateTime") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_PATTERN_REQUEST) LocalDateTime endDateTime,
        @Parameter(description="Параметр запроса для постраничного вывода. Задает номер текущей страницы.",
            example = "1", schema=@Schema(implementation = Integer.class))
        @RequestParam(value = "page", defaultValue = "1") Integer pageNo,
        @Parameter(description="Параметр запроса для постраничного вывода. Задает количество элементов на страницу.",
            example = AppConfig.DEFAULT_PAGE_LIMIT, schema=@Schema(implementation = Integer.class))
        @RequestParam(value = "limit", defaultValue = AppConfig.DEFAULT_PAGE_LIMIT) Integer limit
    )
    {
        checkDateTimeFilter(startDateTime, endDateTime);
        Account account = accountService.get(accountId);
        ResponseDto<ru.resprojects.dfops.model.Operation> response = new ResponseDto<>(
            operationService.getAllByAccountIdBetween(account.getId(), startDateTime, endDateTime, pageNo, limit)
        );
        if (response.getElements().isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        }
        return ResponseEntity.ok(response);
    }

    @Operation(
        summary = "Вывод оборота по операциям за период для лицевого счёта",
        description = "Возвращает оборот по операциям за период для указанного лицевого счёта работника",
        tags = { "operation" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = ResponseDto.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "400",
            description = "некорректный запрос",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "404",
            description = "лицевой счёт не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @GetMapping(value = "/{account_id}/operations_amount/filter", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto<OperationAmountDto>> getOperationsAmountBetween(
        @Parameter(description="ID лицевого счёта", schema=@Schema(implementation = Long.class), required = true, example = "5002")
        @PathVariable("account_id") Long accountId,
        @Parameter(description="Начальная дата и время (формат dd.MM.yyyy-HH:mm)", schema=@Schema(implementation = String.class),
            example = "01.05.2020-00:00", required = true)
        @RequestParam(value = "startDateTime") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_PATTERN_REQUEST) LocalDateTime startDateTime,
        @Parameter(description="Конечная дата и время (формат dd.MM.yyyy-HH:mm)", schema=@Schema(implementation = String.class),
            example = "31.05.2020-23:59", required = true)
        @RequestParam(value = "endDateTime") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_PATTERN_REQUEST) LocalDateTime endDateTime
    )
    {
        checkDateTimeFilter(startDateTime, endDateTime);
        Account account = accountService.get(accountId);
        List<OperationAmountDto> result = operationService.getOperationsAmountBetween(account.getId(), startDateTime, endDateTime);
        ResponseDto<OperationAmountDto> responseDto = new ResponseDto<>();
        responseDto.setElements(result);
        responseDto.setPageCount(1);
        responseDto.setCurrentPage(1);
        responseDto.setTotalItems(result.size());
        return ResponseEntity.ok(responseDto);
    }

    @Operation(
        summary = "Вывод дебет лицевого счёта за период",
        description = "Возвращает дебет лицевого счёта работника за указанный период",
        tags = { "operation" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = OperationAmountDto.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "400",
            description = "некорректный запрос",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "404",
            description = "лицевой счёт не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @GetMapping(value = "/{account_id}/debit/filter", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OperationAmountDto> getDebitBetween(
        @Parameter(description="ID лицевого счёта", schema=@Schema(implementation = Long.class), required = true, example = "5002")
        @PathVariable("account_id") Long accountId,
        @Parameter(description="Начальная дата и время (формат dd.MM.yyyy-HH:mm)", schema=@Schema(implementation = String.class),
            example = "01.05.2020-00:00", required = true)
        @RequestParam(value = "startDateTime") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_PATTERN_REQUEST) LocalDateTime startDateTime,
        @Parameter(description="Конечная дата и время (формат dd.MM.yyyy-HH:mm)", schema=@Schema(implementation = String.class),
            example = "31.05.2020-23:59", required = true)
        @RequestParam(value = "endDateTime") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_PATTERN_REQUEST) LocalDateTime endDateTime
    )
    {
        checkDateTimeFilter(startDateTime, endDateTime);
        Account account = accountService.get(accountId);
        BigDecimal result = operationService.getDebitBetween(account.getId(), startDateTime, endDateTime);
        OperationAmountDto operationAmountDto = new OperationAmountDto(result, ru.resprojects.dfops.model.Operation.OperationType.DEPOSIT);
        return ResponseEntity.ok(operationAmountDto);
    }

    @Operation(
        summary = "Вывод кредит лицевого счёта",
        description = "Возвращает кредит лицевого счёта работника за период",
        tags = { "operation" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = OperationAmountDto.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "400",
            description = "некорректный запрос",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "404",
            description = "лицевой счёт не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @GetMapping(value = "/{account_id}/credit/filter", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OperationAmountDto> getCreditBetween(
        @Parameter(description="ID лицевого счёта", schema=@Schema(implementation = Long.class), required = true, example = "5002")
        @PathVariable("account_id") Long accountId,
        @Parameter(description="Начальная дата и время (формат dd.MM.yyyy-HH:mm)", schema=@Schema(implementation = String.class),
            example = "01.05.2020-00:00", required = true)
        @RequestParam(value = "startDateTime") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_PATTERN_REQUEST) LocalDateTime startDateTime,
        @Parameter(description="Конечная дата и время (формат dd.MM.yyyy-HH:mm)", schema=@Schema(implementation = String.class),
            example = "31.05.2020-23:59", required = true)
        @RequestParam(value = "endDateTime") @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_PATTERN_REQUEST) LocalDateTime endDateTime
    )
    {
        checkDateTimeFilter(startDateTime, endDateTime);
        Account account = accountService.get(accountId);
        BigDecimal result = operationService.getCreditBetween(account.getId(), startDateTime, endDateTime);
        OperationAmountDto operationAmountDto = new OperationAmountDto(result, ru.resprojects.dfops.model.Operation.OperationType.WITHDRAW);
        return ResponseEntity.ok(operationAmountDto);
    }

    @Operation(
        summary = "Текущий баланс лицевого счёта",
        description = "Выводит текущий баланс лицевого счёта",
        tags = { "operation" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = AccountBalanceDto.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "404",
            description = "лицевой счёт не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @GetMapping(value = "/{account_id}/balance", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AccountBalanceDto> getBalance(
        @Parameter(description="ID лицевого счёта", schema=@Schema(implementation = Long.class), required = true, example = "5002")
        @PathVariable("account_id") Long accountId) {
        Account account = accountService.get(accountId);
        BigDecimal balance = operationService.getCurrentBalance(account.getId());
        AccountBalanceDto balanceDto = new AccountBalanceDto(account.getId(), account.getPersonalAccount(), balance);
        return ResponseEntity.ok(balanceDto);
    }

    @Operation(
        summary = "Зачисление на лицевой счёт",
        description = "Зачисление указанной суммы на лицевого счёта работника",
        tags = { "operation" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = ru.resprojects.dfops.model.Operation.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "400",
            description = "некорректный запрос",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "403",
            description = "некорректные данные",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "404",
            description = "лицевой счёт не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @PostMapping(value = "/deposit", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ru.resprojects.dfops.model.Operation> deposit(
        @Parameter(description="Объект с даннами для проведения операции по лицевому счёту", required=true, schema=@Schema(implementation = OperationDto.class))
        @RequestBody OperationDto operationDto
    )
    {
        if (operationDto == null) {
            throw new BadRequestException("Request body is null");
        }
        if (operationDto.getOperationType() == null) {
            throw new BadRequestException("Unknown operation type");
        }
        if (!ru.resprojects.dfops.model.Operation.OperationType.DEPOSIT.equals(operationDto.getOperationType())) {
            throw new BadRequestException("Must be DEPOSIT but " + operationDto.getOperationType());
        }
        Account account = accountService.get(operationDto.getAccountId());
        return ResponseEntity.ok(operationService.deposit(account, operationDto.getAmount()));
    }

    @Operation(
        summary = "Списание с лицевого счёта",
        description = "Списание указанной суммы с лицевого счёта работника",
        tags = { "operation" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция",
            content = @Content(
                schema = @Schema(implementation = ru.resprojects.dfops.model.Operation.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "400",
            description = "некорректный запрос",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "403",
            description = "некорректные данные",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "404",
            description = "лицевой счёт не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @PostMapping(value = "/withdraw", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ru.resprojects.dfops.model.Operation> withdraw(
        @Parameter(description="Объект с даннами для проведения операции по лицевому счёту", required=true, schema=@Schema(implementation = OperationDto.class))
        @RequestBody OperationDto operationDto
    )
    {
        if (operationDto == null) {
            throw new BadRequestException("Request body is null");
        }
        if (operationDto.getOperationType() == null) {
            throw new BadRequestException("Unknown operation type");
        }
        if (!ru.resprojects.dfops.model.Operation.OperationType.WITHDRAW.equals(operationDto.getOperationType())) {
            throw new BadRequestException("Must be WITHDRAW but " + operationDto.getOperationType());
        }
        Account account = accountService.get(operationDto.getAccountId());
        return ResponseEntity.ok(operationService.withdraw(account, operationDto.getAmount()));
    }

    @Operation(
        summary = "Перевод с лицевого счёта на лицевого счёта",
        description = "Перевод с лицевого счёта на лицевой счёт работника",
        tags = { "operation" }
    )
    @ApiResponses(value = {
        @ApiResponse(
            responseCode = "200",
            description = "успешная операция"
        ),
        @ApiResponse(
            responseCode = "400",
            description = "некорректный запрос",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "404",
            description = "лицевой счёт не найден",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "403",
            description = "некорректные данные",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        ),
        @ApiResponse(
            responseCode = "500",
            description = "неизвестная ошибка",
            content = @Content(
                schema = @Schema(implementation = ErrorMessage.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE
            )
        )
    })
    @PostMapping(value = "/transfer", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> transfer(
        @Parameter(description="Объект с даннами для перевода суммы между счетами", required=true, schema=@Schema(implementation = OperationTransferDto.class))
        @RequestBody OperationTransferDto operationTransferDto
    )
    {
        if (operationTransferDto == null) {
            throw new BadResourceException("Request body is null");
        }
        Account from = accountService.get(operationTransferDto.getAccountIdFrom());
        Account to = accountService.get(operationTransferDto.getAccountIdTo());
        operationService.transfer(from, to, operationTransferDto.getAmount());
        return ResponseEntity.ok().body(null);
    }

    private void checkDateTimeFilter(LocalDateTime startDateTime, LocalDateTime endDateTime) {
        if (startDateTime == null || endDateTime == null) {
            throw new BadRequestException("Start date-time or end date-time is null");
        }
        if (startDateTime.compareTo(endDateTime) > 0) {
            throw new BadRequestException("Start date-time later than end date-time");
        }
    }

}
