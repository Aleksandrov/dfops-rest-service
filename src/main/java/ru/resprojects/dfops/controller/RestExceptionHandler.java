package ru.resprojects.dfops.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.resprojects.dfops.exception.BadRequestException;
import ru.resprojects.dfops.exception.BadResourceException;
import ru.resprojects.dfops.exception.ErrorMessage;
import ru.resprojects.dfops.exception.ResourceAlreadyExistsException;
import ru.resprojects.dfops.exception.ResourceNotFoundException;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(value = {ResourceNotFoundException.class})
    public ResponseEntity<ErrorMessage> resourceNotFound(HttpServletRequest request, ResourceNotFoundException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getErrorMessage());
    }

    @ExceptionHandler(value = {BadResourceException.class})
    public ResponseEntity<ErrorMessage> badResource(HttpServletRequest request, BadResourceException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getErrorMessage());
    }

    @ExceptionHandler(value = {BadRequestException.class})
    public ResponseEntity<ErrorMessage> badRequest(HttpServletRequest request, BadRequestException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getErrorMessage());
    }

    @ExceptionHandler(value = {ResourceAlreadyExistsException.class})
    public ResponseEntity<ErrorMessage> alreadyExistResource(HttpServletRequest request, ResourceAlreadyExistsException exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(exception.getErrorMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> handleError(HttpServletRequest requesr, Exception exception) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new ErrorMessage("Unexpected error: " + exception.getMessage()));
    }

}
