package ru.resprojects.dfops.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.resprojects.dfops.model.Account;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {


    Page<Account> findAllByEmployee_Id(long employeeId, Pageable pageable);

    boolean existsByPersonalAccount(String personalAccount);

    @EntityGraph(attributePaths = {"employee"}, type = EntityGraph.EntityGraphType.LOAD)
    @Query("SELECT a FROM Account a WHERE a.id=:id")
    Optional<Account> getWithEmployee(@Param("id") long id);

}
