package ru.resprojects.dfops.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.resprojects.dfops.dto.operation.OperationAmountDto;
import ru.resprojects.dfops.model.Operation;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface OperationRepository extends JpaRepository<Operation, Long> {

    Page<Operation> getAllByAccount_Id(long accountId, Pageable pageable);

    @SuppressWarnings("JpaQlInspection")
    @Query("SELECT o " +
        "FROM Operation o " +
        "WHERE o.account.id=:accountId AND o.dateTime BETWEEN :startDate AND :endDate")
    Page<Operation> getAllByAccountIdBetween(
        @Param("accountId") long accountId,
        @Param("startDate") LocalDateTime startDate,
        @Param("endDate") LocalDateTime endDate,
        Pageable pageable
    );

    @Query("SELECT new ru.resprojects.dfops.dto.operation.OperationAmountDto(sum(o.operationValue), o.operationType) " +
        "FROM Operation o " +
        "WHERE o.account.id=:accountId " +
        "GROUP BY o.operationType")
    List<OperationAmountDto> getOperationsAmount(@Param("accountId") long accountId);

    @SuppressWarnings("JpaQlInspection")
    @Query("SELECT new ru.resprojects.dfops.dto.operation.OperationAmountDto(sum(o.operationValue), o.operationType) " +
        "FROM Operation o " +
        "WHERE o.account.id=:accountId AND o.dateTime BETWEEN :startDate AND :endDate " +
        "GROUP BY o.operationType")
    List<OperationAmountDto> getOperationsAmountBetween(
        @Param("accountId") long accountId,
        @Param("startDate") LocalDateTime startDate,
        @Param("endDate") LocalDateTime endDate
    );


    @SuppressWarnings("JpaQlInspection")
    @Query("SELECT sum(o.operationValue) " +
        "FROM Operation o " +
        "WHERE o.account.id=:accountId AND o.operationType = :operationType AND o.dateTime BETWEEN :startDate AND :endDate " +
        "GROUP BY o.operationType")
    Optional<BigDecimal> getOperationAmountBetween(
        @Param("accountId") long accountId,
        @Param("startDate") LocalDateTime startDate,
        @Param("endDate") LocalDateTime endDate,
        @Param("operationType") Operation.OperationType operationType
    );

}
