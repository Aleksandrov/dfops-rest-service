package ru.resprojects.dfops.service;

import org.springframework.data.domain.Page;
import ru.resprojects.dfops.dto.operation.OperationAmountDto;
import ru.resprojects.dfops.exception.BadResourceException;
import ru.resprojects.dfops.exception.ResourceNotFoundException;
import ru.resprojects.dfops.model.Account;
import ru.resprojects.dfops.model.Operation;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface OperationService {

    Operation deposit(Account account, double amount);

    Operation withdraw(Account account, double amount) throws BadResourceException;

    BigDecimal getCurrentBalance(long accountId);

    BigDecimal getDebitBetween(long accountId, LocalDateTime start, LocalDateTime end) throws ResourceNotFoundException;

    BigDecimal getCreditBetween(long accountId, LocalDateTime start, LocalDateTime end) throws ResourceNotFoundException;

    List<OperationAmountDto> getOperationsAmountBetween(long accountId, LocalDateTime start, LocalDateTime end);

    Page<Operation> getAllByAccountId(long accountId, int pageNumber, int rowPerPage);

    Page<Operation> getAllByAccountIdBetween(long accountId, LocalDateTime start, LocalDateTime end, int pageNumber, int rowPerPage);

    void transfer(Account accountFrom, Account accountTo, double amount) throws BadResourceException;

}
