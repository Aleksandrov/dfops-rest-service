package ru.resprojects.dfops.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.resprojects.dfops.exception.BadResourceException;
import ru.resprojects.dfops.exception.ResourceAlreadyExistsException;
import ru.resprojects.dfops.exception.ResourceNotFoundException;
import ru.resprojects.dfops.model.Account;
import ru.resprojects.dfops.model.Employee;
import ru.resprojects.dfops.repository.AccountRepository;

@Service
@Transactional(readOnly = true)
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Transactional
    @Override
    public Account create(Employee employee, String personalAccount) throws BadResourceException, ResourceAlreadyExistsException {
        if (employee == null) {
            throw new BadResourceException("Failed to save account. Employee is null");
        }
        if (StringUtils.isEmpty(personalAccount)) {
            throw new BadResourceException("Failed to save account. Personal account is null or empty");
        }
        if (accountRepository.existsByPersonalAccount(personalAccount)) {
            String message = String.format(
                "Failed to save account. Bank account with %s for employee id %d is exists",
                personalAccount,
                employee.getId()
            );
            throw new ResourceAlreadyExistsException(message);
        }
        Account account = new Account(employee, personalAccount);
        return accountRepository.save(account);
    }

    @Transactional
    @Override
    public void delete(long id) throws ResourceNotFoundException {
        if (get(id) != null) {
            accountRepository.deleteById(id);
        }
    }

    @Override
    public Account get(long id) throws ResourceNotFoundException {
        return accountRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Cannot find Account with id: " + id));
    }

    @Override
    public Page<Account> getAll(long employeeId, int pageNumber, int rowPerPage) {
        return accountRepository.findAllByEmployee_Id(employeeId, PageRequest.of(pageNumber - 1, rowPerPage));
    }

}
