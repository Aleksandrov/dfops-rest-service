package ru.resprojects.dfops.service;

import org.springframework.data.domain.Page;
import ru.resprojects.dfops.exception.BadResourceException;
import ru.resprojects.dfops.exception.ResourceAlreadyExistsException;
import ru.resprojects.dfops.exception.ResourceNotFoundException;
import ru.resprojects.dfops.model.Employee;

public interface EmployeeService {

    Employee create(Employee employee) throws ResourceAlreadyExistsException, BadResourceException;

    void delete(long id) throws ResourceNotFoundException;

    Employee get(long id) throws ResourceNotFoundException;

    Employee getByEmail(String email) throws ResourceNotFoundException;

    Page<Employee> getAll(int pageNumber, int rowPerPage);

}
