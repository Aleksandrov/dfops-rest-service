package ru.resprojects.dfops.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.resprojects.dfops.dto.operation.OperationAmountDto;
import ru.resprojects.dfops.exception.BadResourceException;
import ru.resprojects.dfops.exception.ResourceNotFoundException;
import ru.resprojects.dfops.model.Account;
import ru.resprojects.dfops.model.Operation;
import ru.resprojects.dfops.repository.OperationRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class OperationServiceImpl implements OperationService {

    private final OperationRepository operationRepository;

    public OperationServiceImpl(OperationRepository operationRepository) {
        this.operationRepository = operationRepository;
    }

    @Transactional
    @Override
    public Operation deposit(Account account, double amount) {
        BigDecimal depositAmount = new BigDecimal(String.valueOf(amount));
        Operation operation = new Operation(account, Operation.OperationType.DEPOSIT, depositAmount.setScale(2, RoundingMode.HALF_UP));
        return operationRepository.save(operation);
    }

    @Transactional
    @Override
    public Operation withdraw(Account account, double amount) throws BadResourceException {
        double currentBalance = getCurrentBalance(account.getId()).doubleValue();
        if (currentBalance < amount) {
            throw new BadResourceException(
                "Cannot withdraw from account = " + account.getPersonalAccount()
                    + " because current balance of account " + account.getPersonalAccount() + "(" + currentBalance + ")"
                    + " < amount = " + amount
            );
        }
        BigDecimal withdrawAmount = new BigDecimal(String.valueOf(amount));
        Operation operation = new Operation(account, Operation.OperationType.WITHDRAW, withdrawAmount.setScale(2, RoundingMode.HALF_UP));
        return operationRepository.save(operation);
    }

    @Override
    public BigDecimal getCurrentBalance(long accountId) {
        List<OperationAmountDto> operationAmountDtos = operationRepository.getOperationsAmount(accountId);
        BigDecimal sum = new BigDecimal("0.00");
        for (OperationAmountDto amountDto : operationAmountDtos) {
            if (Operation.OperationType.WITHDRAW.equals(amountDto.getOperationType())) {
                sum = sum.subtract(amountDto.getSum());
            } else {
                sum = sum.add(amountDto.getSum());
            }
        }
        return sum.setScale(2, RoundingMode.HALF_UP);
    }

    @Override
    public BigDecimal getDebitBetween(long accountId, LocalDateTime start, LocalDateTime end) throws ResourceNotFoundException {
        BigDecimal sum = operationRepository.getOperationAmountBetween(accountId, start, end, Operation.OperationType.DEPOSIT).orElse(new BigDecimal("0.00"));
        return sum.setScale(2, RoundingMode.HALF_UP);
    }

    @Override
    public BigDecimal getCreditBetween(long accountId, LocalDateTime start, LocalDateTime end)  throws ResourceNotFoundException {
        BigDecimal sum = operationRepository.getOperationAmountBetween(accountId, start, end, Operation.OperationType.WITHDRAW).orElse(new BigDecimal("0.00"));
        return sum.setScale(2, RoundingMode.HALF_UP);
    }

    @Override
    public List<OperationAmountDto> getOperationsAmountBetween(long accountId, LocalDateTime start, LocalDateTime end) {
        return operationRepository.getOperationsAmountBetween(accountId, start, end);
    }

    @Override
    public Page<Operation> getAllByAccountId(long accountId, int pageNumber, int rowPerPage) {
        return operationRepository.getAllByAccount_Id(accountId, PageRequest.of(pageNumber - 1, rowPerPage));
    }

    @Override
    public Page<Operation> getAllByAccountIdBetween(long accountId, LocalDateTime start, LocalDateTime end, int pageNumber, int rowPerPage) {
        return operationRepository.getAllByAccountIdBetween(accountId, start, end, PageRequest.of(pageNumber - 1, rowPerPage));
    }

    @Transactional
    @Override
    public void transfer(Account accountFrom, Account accountTo, double amount) throws BadResourceException {
        if (!accountFrom.getEmployee().getId().equals(accountTo.getEmployee().getId())) {
            throw new BadResourceException("Accounts from and account to do not belong to one employee");
        }
        withdraw(accountFrom, amount);
        deposit(accountTo, amount);
    }

}
