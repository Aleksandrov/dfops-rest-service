package ru.resprojects.dfops.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.resprojects.dfops.exception.BadResourceException;
import ru.resprojects.dfops.exception.ResourceAlreadyExistsException;
import ru.resprojects.dfops.exception.ResourceNotFoundException;
import ru.resprojects.dfops.model.Employee;
import ru.resprojects.dfops.repository.EmployeeRepository;

@Service
@Transactional(readOnly = true)
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Transactional
    @Override
    public Employee create(Employee employee) throws ResourceAlreadyExistsException, BadResourceException {
        if (employee == null) {
            throw new BadResourceException("Failed to save employee. Employee is null");
        }
        String email = employee.getEmail();
        if (StringUtils.isEmpty(email)) {
            throw new BadResourceException("Failed to save employee. Employee e-mail is null or empty");
        }
        if (employeeRepository.existsByEmail(email)) {
            throw new ResourceAlreadyExistsException("Failed to save employee. Employee with email is exists");
        }
        return employeeRepository.save(employee);
    }

    @Transactional
    @Override
    public void delete(long id) throws ResourceNotFoundException {
        if (get(id) != null) {
            employeeRepository.deleteById(id);
        }
    }

    @Override
    public Employee get(long id) throws ResourceNotFoundException {
        return employeeRepository.findById(id).orElseThrow(
            () -> new ResourceNotFoundException("Cannot find Employee with id: " + id)
        );
    }

    @Override
    public Employee getByEmail(String email) throws ResourceNotFoundException {
        return employeeRepository.findByEmail(email).orElseThrow(
            () -> new ResourceNotFoundException("Cannot find Employee with e-mail: " + email)
        );
    }

    @Override
    public Page<Employee> getAll(int pageNumber, int rowPerPage) {
        return employeeRepository.findAll(PageRequest.of(pageNumber - 1, rowPerPage));
    }

}
