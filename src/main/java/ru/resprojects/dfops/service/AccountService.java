package ru.resprojects.dfops.service;

import org.springframework.data.domain.Page;
import ru.resprojects.dfops.exception.BadResourceException;
import ru.resprojects.dfops.exception.ResourceAlreadyExistsException;
import ru.resprojects.dfops.exception.ResourceNotFoundException;
import ru.resprojects.dfops.model.Account;
import ru.resprojects.dfops.model.Employee;


public interface AccountService {

    Account create(Employee employee, String personalAccount) throws BadResourceException, ResourceAlreadyExistsException;

    void delete(long id) throws ResourceNotFoundException;

    Account get(long id) throws ResourceNotFoundException;

    Page<Account> getAll(long employeeId, int pageNumber, int rowPerPage);

}
