package ru.resprojects.dfops.dto.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Schema(description = "Объект для передачи данных о текущем балансе лицевого счёта")
public class AccountBalanceDto implements Serializable {

    private static final long serialVersionUID = -82064792530944046L;

    @Schema(description = "ID лицевого счёта")
    @JsonProperty("account_id")
    private Long accountId;

    @Schema(description = "Номер лицевого счёта")
    @JsonProperty("personal_account")
    private String personalAccount;

    @Schema(description = "Текущий баланс лицевого счёта")
    @JsonProperty("balance")
    private BigDecimal balance;

}
