package ru.resprojects.dfops.dto.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Schema(description = "Объект для передачи данных о регистрации лицевого счёта для выбранного работника")
public class AccountDto implements Serializable {

    private static final long serialVersionUID = 8356762606534341601L;

    @Schema(description = "id работника", example = "5000", required = true)
    @JsonProperty("employee_id")
    @NotNull
    private Long employeeId;

    @Schema(description = "лицевой счёт работника", example = "123456789", required = true)
    @JsonProperty("personal_account")
    @NotBlank
    private String personalAccount;

}
