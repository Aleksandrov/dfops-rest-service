package ru.resprojects.dfops.dto.employee;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Schema(description = "Объект для передачи данных о регистрации работника в БД")
public class EmployeeDto implements Serializable {

    private static final long serialVersionUID = 313229127008191126L;

    @Schema(description = "Имя работника", example = "Alex", required = true)
    @JsonProperty("name")
    @NotBlank
    private String name;

    @Schema(description = "E-mail работника", example = "alex@example.com", required = true)
    @JsonProperty("email")
    @NotBlank
    private String email;

}
