package ru.resprojects.dfops.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.Collection;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Schema(description = "Объект для постраничного вывода набора элементов")
public class ResponseDto<T> implements Serializable {

    private static final long serialVersionUID = -3654370454854576301L;

    @Schema(description = "Список элементов")
    @JsonProperty("elements")
    private Collection<T> elements;

    @Schema(description = "Номер текущей страницы")
    @JsonProperty("currentPage")
    private int currentPage;

    @Schema(description = "Всего элементов")
    @JsonProperty("totalItems")
    private long totalItems;

    @Schema(description = "Номер всего страниц")
    @JsonProperty("totalPages")
    private int pageCount;

    public ResponseDto(Page<T> response) {
        this.elements = response.getContent();
        this.currentPage = response.getNumber() + 1;
        this.totalItems = response.getTotalElements();
        this.pageCount = response.getTotalPages();
    }
}
