package ru.resprojects.dfops.dto.operation;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.resprojects.dfops.model.Operation;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Schema(description = "Операция по лицевому счёту")
public class OperationDto implements Serializable {

    private static final long serialVersionUID = 6502515027792967205L;

    @Schema(description = "ID лицевого счёта", example = "5002", required = true)
    @JsonProperty("account_id")
    private Long accountId;

    @Schema(description = "Сумма операции", example = "100.25", required = true)
    @JsonProperty("operation_amount")
    double amount;

    @Schema(description = "Тип операции: DEPOSIT - начисление, WITHDRAW - списание", example = "WITHDRAW", required = true)
    @JsonProperty("operation_type")
    Operation.OperationType operationType;

}
