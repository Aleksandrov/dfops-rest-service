package ru.resprojects.dfops.dto.operation;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.resprojects.dfops.model.Operation;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Объект для передачи данных о сумме и типе операций по счёту")
public class OperationAmountDto implements Serializable {

    private static final long serialVersionUID = 3448183919367413609L;

    @Schema(description = "Сумма")
    @JsonProperty("operation_sum")
    private BigDecimal sum;

    @Schema(description = "Тип операции: WITHDRAW - списание, DEPOSIT - зачисление")
    @JsonProperty("operation_type")
    private Operation.OperationType operationType;

}
