package ru.resprojects.dfops.dto.operation;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Schema(description = "Перевод между лицевыми счетами работника. ID лицевых счетов " +
    "откуда переводят и куда переводят должны принадлежать одному и тому же работнику")
public class OperationTransferDto implements Serializable {

    private static final long serialVersionUID = 5904536254136545140L;

    @Schema(description = "ID лицевого счёта откуда переводят", example = "5002", required = true)
    @JsonProperty("account_id_from")
    private Long accountIdFrom;

    @Schema(description = "ID лицевого счёта куда переводят", example = "5003", required = true)
    @JsonProperty("account_id_to")
    private Long accountIdTo;

    @Schema(description = "Переводимая сумма", example = "100.00", required = true)
    @JsonProperty("operation_amount")
    double amount;

}
