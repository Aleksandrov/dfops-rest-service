package ru.resprojects.dfops.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "employee_personal_accounts", uniqueConstraints = {
    @UniqueConstraint(
        columnNames = {"employee_id", "personal_account"},
        name = "employee_personal_accounts_unique_employee_account_idx"
    )
})
@NoArgsConstructor
@Getter
@Setter
@Schema(description = "Счёт работника")
public class Account extends AbstractBaseEntity {

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Employee employee;

    @Schema(description = "Лицевой счёт работника")
    @Column(name = "personal_account", nullable = false, unique = true)
    @NotBlank
    @Size(min = 2, max = 50)
    @JsonProperty("personal_account")
    private String personalAccount;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    private List<Operation> operations;

    public Account(Employee employee, String personalAccount) {
        this(null, employee, personalAccount);
    }

    public Account(Long id, Employee employee, String personalAccount) {
        super(id);
        this.employee = employee;
        this.personalAccount = personalAccount;
    }

    @Override
    public String toString() {
        return "Account{" +
            "personalAccount='" + personalAccount + '\'' +
            ", id=" + id +
            '}';
    }

}
