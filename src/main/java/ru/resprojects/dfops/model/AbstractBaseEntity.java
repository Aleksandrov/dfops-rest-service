package ru.resprojects.dfops.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;

@MappedSuperclass
@Access(AccessType.FIELD)
@Getter
@Setter
@EqualsAndHashCode
@ToString
public abstract class AbstractBaseEntity {

    public static final int START_SEQ = 5000;

    @Schema(description = "Уникальный идентификатор")
    @Id
    @SequenceGenerator(
        name = "seq_employees",
        sequenceName = "seq_employees",
        allocationSize = 1,
        initialValue = START_SEQ
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_employees")
    @JsonProperty("id")
    protected Long id;

    protected AbstractBaseEntity() {
    }

    protected AbstractBaseEntity(Long id) {
        this.id = id;
    }

}