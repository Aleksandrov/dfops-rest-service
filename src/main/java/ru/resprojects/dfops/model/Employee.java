package ru.resprojects.dfops.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "employees", uniqueConstraints = {@UniqueConstraint(columnNames = "email", name = "employees_unique_email_idx")})
@NoArgsConstructor
@Getter
@Setter
@ToString(of = {"name", "email"})
@Schema(description = "Работник")
public class Employee extends AbstractBaseEntity {

    @Schema(description = "Имя работника")
    @Column(name = "name", nullable = false)
    @NotBlank
    @Size(min = 2, max = 100)
    @JsonProperty("name")
    private String name;

    @Schema(description = "E-mail работника")
    @Column(name = "email", nullable = false, unique = true)
    @Email
    @NotBlank
    @Size(max = 100)
    @JsonProperty("email")
    private String email;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
    private List<Account> accounts;

    public Employee(String name, String email) {
        this(null, name, email);
    }

    public Employee(Long id, String name, String email) {
        super(id);
        this.name = name;
        this.email = email;
    }
}
