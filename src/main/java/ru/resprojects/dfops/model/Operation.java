package ru.resprojects.dfops.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;
import ru.resprojects.dfops.util.DateTimeUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "personal_account_operations", uniqueConstraints = {
    @UniqueConstraint(
        columnNames = {"personal_account_id", "operation_date_time"},
        name = "personal_account_operations_unique_account_datetime_idx"
    )
})
@NoArgsConstructor
@Getter
@Setter
@Schema(description = "Операция по лицевому счёту")
public class Operation extends AbstractBaseEntity {

    //TODO найти как отобразить в OpenApi Enum
    public static enum OperationType {
        DEPOSIT,
        WITHDRAW
    }

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "personal_account_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Account account;

    @Schema(description = "Дата и время операции")
    @Column(name = "operation_date_time", nullable = false)
    @NotNull
    @DateTimeFormat(pattern = DateTimeUtil.DATE_TIME_PATTERN)
    @JsonProperty("operation_date_time")
    private LocalDateTime dateTime;

    @Schema(description = "Тип операции")
    @Column(name = "operation_type", nullable = false)
    @Enumerated(EnumType.STRING)
    @NotNull
    @JsonProperty("operation_type")
    private OperationType operationType;

    @Schema(description = "Сумма операции")
    @Column(name = "operation_value", nullable = false)
    @NotNull
    @JsonProperty("operation_amount")
    private BigDecimal operationValue;

    public Operation(Account account, OperationType operationType, BigDecimal operationValue) {
        this(null, account, operationType, operationValue);
    }

    public Operation(Long id, Account account, OperationType operationType, BigDecimal operationValue) {
        super(id);
        this.account = account;
        this.dateTime = LocalDateTime.now();
        this.operationType = operationType;
        this.operationValue = operationValue;
    }

    @Override
    public String toString() {
        return "Operation{" +
            " dateTime=" + dateTime +
            ", operationType=" + operationType +
            ", operationValue=" + operationValue +
            ", id=" + id +
            '}';
    }

}
