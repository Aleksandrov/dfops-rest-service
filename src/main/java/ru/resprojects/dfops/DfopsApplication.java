package ru.resprojects.dfops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DfopsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DfopsApplication.class, args);
    }

}
