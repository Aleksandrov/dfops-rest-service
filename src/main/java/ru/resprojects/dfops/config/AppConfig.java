package ru.resprojects.dfops.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    public static final String DEFAULT_PAGE_LIMIT = "10";

    @Bean
    public OpenAPI customOpenApi() {
        return new OpenAPI()
            .components(new Components())
            .info(new Info().title("Driver Finance Operation REST Service API").description(
                "REST-сервис для приема и регистрации операций в базе данных по расчетам с водителями."));
    }

}
