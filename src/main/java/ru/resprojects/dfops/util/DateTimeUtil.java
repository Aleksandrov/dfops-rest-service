package ru.resprojects.dfops.util;

import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public final class DateTimeUtil {

    public static final String DATE_TIME_PATTERN = "dd.MM.yyyy HH:mm";
    public static final String DATE_TIME_PATTERN_REQUEST = "dd.MM.yyyy-HH:mm";
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);

    private DateTimeUtil() {
    }

    public static String toString(LocalDateTime localDateTime) {
        return localDateTime == null ? "" : localDateTime.format(DATE_TIME_FORMATTER);
    }

    public static LocalDate parseLocalDate(String localDateString) {
        return StringUtils.isEmpty(localDateString) ? null : LocalDate.parse(localDateString);
    }

    public static LocalTime parseLocalTime(String localTimeString) {
        return StringUtils.isEmpty(localTimeString) ? null : LocalTime.parse(localTimeString);
    }

}
