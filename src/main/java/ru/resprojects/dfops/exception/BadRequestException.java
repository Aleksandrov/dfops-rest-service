package ru.resprojects.dfops.exception;

public class BadRequestException extends RuntimeException {

    public BadRequestException() {
    }

    public BadRequestException(String msg) {
        super(msg);
    }

    public ErrorMessage getErrorMessage() {
        return new ErrorMessage(this.getMessage());
    }

}
