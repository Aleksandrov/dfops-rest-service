package ru.resprojects.dfops.exception;

public class ResourceAlreadyExistsException extends RuntimeException {

    public ResourceAlreadyExistsException() {
    }

    public ResourceAlreadyExistsException(String msg) {
        super(msg);
    }

    public ErrorMessage getErrorMessage() {
        return new ErrorMessage(this.getMessage());
    }
}
