package ru.resprojects.dfops.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Форма вывода ошибки")
public class ErrorMessage implements Serializable {

    private static final long serialVersionUID = 4089956800265998558L;

    @Schema(description = "Сообщение о ошибке")
    @JsonProperty("msg")
    private String message;

}
