package ru.resprojects.dfops.exception;

public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException() {
    }

    public ResourceNotFoundException(String msg) {
        super(msg);
    }

    public ErrorMessage getErrorMessage() {
        return new ErrorMessage(this.getMessage());
    }

}
