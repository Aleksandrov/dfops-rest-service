package ru.resprojects.dfops.exception;

public class BadResourceException extends RuntimeException {

    public BadResourceException() {
    }

    public BadResourceException(String msg) {
        super(msg);
    }

    public ErrorMessage getErrorMessage() {
        return new ErrorMessage(this.getMessage());
    }

}
