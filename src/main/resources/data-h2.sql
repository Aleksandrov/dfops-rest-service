DELETE FROM personal_account_operations;
DELETE FROM employee_personal_accounts;
DELETE FROM employees;
ALTER SEQUENCE seq_employees RESTART WITH 5000;

INSERT INTO employees (name, email) VALUES
('Ivanov Ivan Ivanovich', 'ivanov@example.com'),
('Petrov Vasily Victorovich', 'petrov@example.com');

INSERT INTO employee_personal_accounts (personal_account, employee_id)
VALUES ('4154014152522741', 5000),
       ('4131668358915203', 5000),
       ('4281563275602455', 5000),
       ('4103234971123321', 5001),
       ('4132555843841699', 5001);

INSERT INTO personal_account_operations (operation_date_time, operation_type, operation_value, personal_account_id)
VALUES (parsedatetime('2020-05-30 10:00:00', 'yyyy-MM-dd hh:mm:ss'), 'DEPOSIT', 840.35, 5002),
       (parsedatetime('2020-05-28 11:05:10', 'yyyy-MM-dd hh:mm:ss'), 'DEPOSIT', 625.00, 5002),
       (parsedatetime('2020-05-25 11:41:10', 'yyyy-MM-dd hh:mm:ss'), 'DEPOSIT', 1080.45, 5002),
       (parsedatetime('2020-05-30 14:00:10', 'yyyy-MM-dd hh:mm:ss'), 'WITHDRAW', 652.33, 5002),
       (parsedatetime('2020-05-26 18:10:10', 'yyyy-MM-dd hh:mm:ss'), 'WITHDRAW', 420.00, 5002),
       (parsedatetime('2020-06-30 10:00:00', 'yyyy-MM-dd hh:mm:ss'), 'DEPOSIT', 1500.52, 5003),
       (parsedatetime('2020-06-30 11:05:10', 'yyyy-MM-dd hh:mm:ss'), 'DEPOSIT', 800.73, 5003),
       (parsedatetime('2020-06-30 14:00:10', 'yyyy-MM-dd hh:mm:ss'), 'WITHDRAW', 170.35, 5003),
       (parsedatetime('2020-06-30 18:10:10', 'yyyy-MM-dd hh:mm:ss'), 'WITHDRAW', 320.00, 5003),
       (parsedatetime('2020-07-15 12:05:10', 'yyyy-MM-dd hh:mm:ss'), 'DEPOSIT', 800.73, 5004),
       (parsedatetime('2020-07-15 12:41:10', 'yyyy-MM-dd hh:mm:ss'), 'DEPOSIT', 350.00, 5004),
       (parsedatetime('2020-07-15 15:00:10', 'yyyy-MM-dd hh:mm:ss'), 'WITHDRAW', 900.35, 5004),
       (parsedatetime('2020-07-15 17:10:10', 'yyyy-MM-dd hh:mm:ss'), 'WITHDRAW', 600.00, 5004),
       (parsedatetime('2020-05-15 11:05:10', 'yyyy-MM-dd hh:mm:ss'), 'DEPOSIT', 976.33, 5005),
       (parsedatetime('2020-05-15 11:41:10', 'yyyy-MM-dd hh:mm:ss'), 'DEPOSIT', 850.00, 5005),
       (parsedatetime('2020-05-15 14:00:10', 'yyyy-MM-dd hh:mm:ss'), 'WITHDRAW', 200.00, 5005),
       (parsedatetime('2020-05-15 18:10:10', 'yyyy-MM-dd hh:mm:ss'), 'WITHDRAW', 375.85, 5005),
       (parsedatetime('2020-04-30 09:00:00', 'yyyy-MM-dd hh:mm:ss'), 'DEPOSIT', 1200.52, 5006),
       (parsedatetime('2020-04-30 10:35:00', 'yyyy-MM-dd hh:mm:ss'), 'DEPOSIT', 300.53, 5006),
       (parsedatetime('2020-04-30 10:55:00', 'yyyy-MM-dd hh:mm:ss'), 'DEPOSIT', 450.60, 5006),
       (parsedatetime('2020-04-30 12:20:10', 'yyyy-MM-dd hh:mm:ss'), 'WITHDRAW', 300.00, 5006),
       (parsedatetime('2020-04-30 14:10:10', 'yyyy-MM-dd hh:mm:ss'), 'WITHDRAW', 402.95, 5006);