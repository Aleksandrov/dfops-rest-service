package ru.resprojects.dfops.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringRunner;
import ru.resprojects.dfops.dto.ResponseDto;
import ru.resprojects.dfops.dto.account.AccountBalanceDto;
import ru.resprojects.dfops.dto.operation.OperationAmountDto;
import ru.resprojects.dfops.dto.operation.OperationDto;
import ru.resprojects.dfops.dto.operation.OperationTransferDto;
import ru.resprojects.dfops.exception.ErrorMessage;
import ru.resprojects.dfops.model.Account;
import ru.resprojects.dfops.model.Employee;
import ru.resprojects.dfops.model.Operation;
import ru.resprojects.dfops.service.AccountService;
import ru.resprojects.dfops.service.OperationService;
import ru.resprojects.dfops.util.DateTimeUtil;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = {"test"})
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
    scripts = {"classpath:schema-h2.sql", "classpath:data-h2.sql"},
    config = @SqlConfig(encoding = "UTF-8"))
public class OperationControllerTest {

    private static final Logger log = LoggerFactory.getLogger(OperationControllerTest.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private OperationService operationService;

    @Autowired
    private AccountService accountService;

    @Test
    public void whenTransferFromOneAccountToAnotherAccountEmployeeThenReturnStatus200() {
        OperationTransferDto transferDto = new OperationTransferDto(5002L, 5003L, 1.0);

        ResponseEntity<Void> response = restTemplate.postForEntity("/rest/v1/operation/transfer", transferDto, Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void whenTryTransferFromOneAccountEmployeeOneToAnotherAccountEmployeeTwoThenReturnStatus400() {
        OperationTransferDto transferDto = new OperationTransferDto(5002L, 5006L, 1.0);

        ResponseEntity<Void> response = restTemplate.postForEntity("/rest/v1/operation/transfer", transferDto, Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void whenTryTransferFromOneAccountToAnotherAccountWithIncorrectAccountIdThenReturnStatus400() {
        OperationTransferDto transferDto = new OperationTransferDto(null, 5006L, 1.0);

        ResponseEntity<Void> response = restTemplate.postForEntity("/rest/v1/operation/transfer", transferDto, Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void whenTryTransferFromOneAccountToAnotherAccountWithNonexistentAccountIdThenReturnStatus404() {
        OperationTransferDto transferDto = new OperationTransferDto(1L, 5006L, 1.0);

        ResponseEntity<Void> response = restTemplate.postForEntity("/rest/v1/operation/transfer", transferDto, Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void whenRequestWithdrawToAccountThenReturnStatus200() {
        OperationDto operationDto = new OperationDto(5002L, 1L, Operation.OperationType.WITHDRAW);

        ResponseEntity<Operation> response = restTemplate.postForEntity("/rest/v1/operation/withdraw", operationDto, Operation.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void whenRequestWithdrawWithNonexistentAccountIdThenReturnStatus404() {
        OperationDto operationDto = new OperationDto(1010L, 1L, Operation.OperationType.WITHDRAW);

        ResponseEntity<Operation> response = restTemplate.postForEntity("/rest/v1/operation/withdraw", operationDto, Operation.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void whenRequestWithdrawWithIncorrectAccountIdThenReturnStatus403() {
        OperationDto operationDto = new OperationDto(null, 1L, Operation.OperationType.WITHDRAW);

        ResponseEntity<Operation> response = restTemplate.postForEntity("/rest/v1/operation/withdraw", operationDto, Operation.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void whenRequestWithdrawWithIncorrectOperationTypeIdThenReturnStatus400() {
        OperationDto operationDto = new OperationDto(5005L, 1L, Operation.OperationType.DEPOSIT);

        ResponseEntity<Operation> response = restTemplate.postForEntity("/rest/v1/operation/withdraw", operationDto, Operation.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void whenRequestDepositToAccountThenReturnStatus200() {
        OperationDto operationDto = new OperationDto(5002L, 1L, Operation.OperationType.DEPOSIT);

        ResponseEntity<Operation> response = restTemplate.postForEntity("/rest/v1/operation/deposit", operationDto, Operation.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void whenRequestDepositWithNonexistentAccountIdThenReturnStatus404() {
        OperationDto operationDto = new OperationDto(1010L, 1L, Operation.OperationType.DEPOSIT);

        ResponseEntity<Operation> response = restTemplate.postForEntity("/rest/v1/operation/deposit", operationDto, Operation.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void whenRequestDepositWithIncorrectAccountIdThenReturnStatus403() {
        OperationDto operationDto = new OperationDto(null, 1L, Operation.OperationType.DEPOSIT);

        ResponseEntity<Operation> response = restTemplate.postForEntity("/rest/v1/operation/deposit", operationDto, Operation.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void whenRequestDepositWithIncorrectOperationTypeIdThenReturnStatus400() {
        OperationDto operationDto = new OperationDto(null, 1L, Operation.OperationType.WITHDRAW);

        ResponseEntity<Operation> response = restTemplate.postForEntity("/rest/v1/operation/deposit", operationDto, Operation.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void whenGetAllOperationsForAccountThenReturnStatus200AndNotNullOperationList() {
        ParameterizedTypeReference<ResponseDto<Operation>> responseType = new ParameterizedTypeReference<>() { };

        ResponseEntity<ResponseDto<Operation>> response = restTemplate.exchange("/rest/v1/operation/5002", HttpMethod.GET, null, responseType);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getElements()).isNotEmpty();
    }

    @Test
    public void whenNoOperationsForAccountThenReturnStatus204() {
        Account account = accountService.create(new Employee(5000L, "1", "1"), "123");
        ParameterizedTypeReference<ResponseDto<Operation>> responseType = new ParameterizedTypeReference<>() { };

        ResponseEntity<ResponseDto<Operation>> response = restTemplate.exchange("/rest/v1/operation/" + account.getId(), HttpMethod.GET, null, responseType);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void whenTryGetAllOperationsForNonexistentAccountThenReturnStatus404() {
        ParameterizedTypeReference<ResponseDto<Operation>> responseType = new ParameterizedTypeReference<>() { };

        ResponseEntity<ResponseDto<Operation>> response = restTemplate.exchange("/rest/v1/operation/1", HttpMethod.GET, null, responseType);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void whenGetCurrentBalanceForAccountThenReturnStatus200AndCurrentBalance() {
        BigDecimal balanceAccount5002 = operationService.getCurrentBalance(5002);

        ResponseEntity<AccountBalanceDto> response = restTemplate.getForEntity("/rest/v1/operation/5002/balance", AccountBalanceDto.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getBalance()).isEqualByComparingTo(balanceAccount5002);
    }

    @Test
    public void whenGetCreditForPeriodForAccountThenReturnStatus200AndCredit() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern(DateTimeUtil.DATE_TIME_PATTERN_REQUEST);
        LocalDateTime start = LocalDateTime.of(2020, Month.MAY, 1, 0, 0);
        LocalDateTime end = LocalDateTime.of(2020, Month.MAY, 31, 23, 59);
        String startDateTime = start.format(format);
        String endDateTime = end.format(format);
        BigDecimal creditAccount5002 = operationService.getCreditBetween(5002, start, end);

        ResponseEntity<OperationAmountDto> response = restTemplate.getForEntity(
            "/rest/v1/operation/5002/credit/filter?startDateTime=" + startDateTime + "&endDateTime=" + endDateTime,
            OperationAmountDto.class
        );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getOperationType()).isEqualTo(Operation.OperationType.WITHDRAW);
        assertThat(response.getBody().getSum()).isEqualByComparingTo(creditAccount5002);
    }

    @Test
    public void whenGetCreditForPeriodWithEndDateTimeLaterThanStartDateTimeThenReturnStatus400() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern(DateTimeUtil.DATE_TIME_PATTERN_REQUEST);
        LocalDateTime start = LocalDateTime.of(2020, Month.MAY, 1, 0, 0);
        LocalDateTime end = LocalDateTime.of(2020, Month.MAY, 31, 23, 59);
        String startDateTime = end.format(format);
        String endDateTime = start.format(format);

        ResponseEntity<ErrorMessage> response = restTemplate.getForEntity(
            "/rest/v1/operation/5002/credit/filter?startDateTime=" + startDateTime + "&endDateTime=" + endDateTime,
            ErrorMessage.class
        );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void whenGetCreditForPeriodWithEmptyStartDateTimeThenReturnStatus400() {
        String startDateTime = "";
        String endDateTime = "";

        ResponseEntity<ErrorMessage> response = restTemplate.getForEntity(
            "/rest/v1/operation/5002/credit/filter?startDateTime=" + startDateTime + "&endDateTime=" + endDateTime,
            ErrorMessage.class
        );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void whenGetCreditForPeriodWhereNoOperationWithdrawThenReturnStatus200AndCreditZero() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern(DateTimeUtil.DATE_TIME_PATTERN_REQUEST);
        LocalDateTime start = LocalDateTime.of(2020, Month.JANUARY, 1, 0, 0);
        LocalDateTime end = LocalDateTime.of(2020, Month.JANUARY, 31, 23, 59);
        String startDateTime = start.format(format);
        String endDateTime = end.format(format);

        ResponseEntity<OperationAmountDto> response = restTemplate.getForEntity(
            "/rest/v1/operation/5002/credit/filter?startDateTime=" + startDateTime + "&endDateTime=" + endDateTime,
            OperationAmountDto.class
        );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getOperationType()).isEqualTo(Operation.OperationType.WITHDRAW);
        assertThat(response.getBody().getSum()).isEqualByComparingTo(new BigDecimal("0.00"));
    }

    @Test
    public void whenGetOperationsAmountForPeriodThenReturnStatus200AndOperationsAmount() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern(DateTimeUtil.DATE_TIME_PATTERN_REQUEST);
        LocalDateTime start = LocalDateTime.of(2020, Month.MAY, 1, 0, 0);
        LocalDateTime end = LocalDateTime.of(2020, Month.MAY, 31, 23, 59);
        String startDateTime = start.format(format);
        String endDateTime = end.format(format);
        ParameterizedTypeReference<ResponseDto<OperationAmountDto>> responseType = new ParameterizedTypeReference<>() { };
        List<OperationAmountDto> operationAmountDtos = operationService.getOperationsAmountBetween(5002, start, end);

        ResponseEntity<ResponseDto<OperationAmountDto>> response = restTemplate.exchange(
            "/rest/v1/operation/5002/operations_amount/filter?startDateTime=" + startDateTime + "&endDateTime=" + endDateTime,
            HttpMethod.GET,
            null,
            responseType
        );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getElements()).isNotEmpty();
        assertThat(response.getBody().getElements()).isSubsetOf(operationAmountDtos);
    }

    @Test
    public void whenGetOperationsAmountForPeriodWhereNoDepositAndWithdrawThenReturnStatus200AndEmptyList() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern(DateTimeUtil.DATE_TIME_PATTERN_REQUEST);
        LocalDateTime start = LocalDateTime.of(2020, Month.JANUARY, 1, 0, 0);
        LocalDateTime end = LocalDateTime.of(2020, Month.JANUARY, 31, 23, 59);
        String startDateTime = start.format(format);
        String endDateTime = end.format(format);
        ParameterizedTypeReference<ResponseDto<OperationAmountDto>> responseType = new ParameterizedTypeReference<>() { };

        ResponseEntity<ResponseDto<OperationAmountDto>> response = restTemplate.exchange(
            "/rest/v1/operation/5002/operations_amount/filter?startDateTime=" + startDateTime + "&endDateTime=" + endDateTime,
            HttpMethod.GET,
            null,
            responseType
        );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getElements()).isEmpty();
    }

}
