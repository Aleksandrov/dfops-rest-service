package ru.resprojects.dfops.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringRunner;
import ru.resprojects.dfops.dto.ResponseDto;
import ru.resprojects.dfops.dto.employee.EmployeeDto;
import ru.resprojects.dfops.exception.ResourceNotFoundException;
import ru.resprojects.dfops.model.Employee;
import ru.resprojects.dfops.service.EmployeeService;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = {"test"})
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
    scripts = {"classpath:schema-h2.sql", "classpath:data-h2.sql"},
    config = @SqlConfig(encoding = "UTF-8"))
public class EmployeeControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void whenRequestGetAllEmployeesThenReturnStatus200AndNotEmptyElementList() {
        ParameterizedTypeReference<ResponseDto<Employee>> responseType = new ParameterizedTypeReference<>() { };

        ResponseEntity<ResponseDto<Employee>> response = restTemplate.exchange("/rest/v1/employee/", HttpMethod.GET, null, responseType);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getElements()).isNotEmpty();
    }

    @Test
    public void whenRequestGetAllEmployeesWithPageLimitThenReturnStatus200AndNotEmptyElementList() {
        ParameterizedTypeReference<ResponseDto<Employee>> responseType = new ParameterizedTypeReference<>() { };

        ResponseEntity<ResponseDto<Employee>> response = restTemplate.exchange("/rest/v1/employee/?page=1&limit=1", HttpMethod.GET, null, responseType);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getElements()).isNotEmpty();
        assertThat(response.getBody().getPageCount()).isEqualTo(2);
    }

    @Test
    public void whenRequestGetEmployeeByIdThenReturnStatus200AndEmployee() {
        Employee employee = employeeService.get(5001);

        ResponseEntity<Employee> response = restTemplate.getForEntity("/rest/v1/employee/get/5001", Employee.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getName()).isEqualTo(employee.getName());
    }

    @Test
    public void whenTryRequestGetEmployeeByIdWithIncorrectIdThenReturnStatus404() {
        ResponseEntity<Employee> response = restTemplate.getForEntity("/rest/v1/employee/get/1", Employee.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void whenRequestGetEmployeeByEmailThenReturnStatus200AndEmployee() {
        Employee employee = employeeService.getByEmail("ivanov@example.com");

        ResponseEntity<Employee> response = restTemplate.getForEntity("/rest/v1/employee/get?email=ivanov@example.com", Employee.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getId()).isEqualTo(employee.getId());
    }

    @Test
    public void whenTryRequestGetEmployeeByEmailWithIncorrectEmailThenReturnStatus404() {
        ResponseEntity<Employee> response = restTemplate.getForEntity("/rest/v1/employee/get?email=123@example.com", Employee.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void whenRequestCreateEmployeeThenReturnStatus200AndCreatedEmployeeWithId() {
        EmployeeDto employeeDto = new EmployeeDto("Alex", "alex@example.com");
        ResponseEntity<Employee> response = restTemplate.postForEntity("/rest/v1/employee/create", employeeDto, Employee.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getId()).isNotNull();
    }

    @Test
    public void whenTryRequestCreateEmployeeWithIncorrectBodyThenReturnStatus403() {
        //EmployeeDto employeeDto = new EmployeeDto("Alex", "alex@example.com");
        ResponseEntity<Employee> response = restTemplate.postForEntity("/rest/v1/employee/create", null, Employee.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void whenTryRequestCreateEmployeeWithEmptyNameThenReturnStatus403() {
        EmployeeDto employeeDto = new EmployeeDto(null, "alex@example.com");
        ResponseEntity<Employee> response = restTemplate.postForEntity("/rest/v1/employee/create", null, Employee.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void whenTryRequestCreateEmployeeWithEmptyEmailThenReturnStatus403() {
        EmployeeDto employeeDto = new EmployeeDto("Alex", null);
        ResponseEntity<Employee> response = restTemplate.postForEntity("/rest/v1/employee/create", null, Employee.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void whenRequestDeleteEmployeeThenReturnDeleteEmployeeFromDb() {
        restTemplate.delete("/rest/v1/employee/5001");

        employeeService.get(5001);
    }


}
