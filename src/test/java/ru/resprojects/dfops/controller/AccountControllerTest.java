package ru.resprojects.dfops.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringRunner;
import ru.resprojects.dfops.dto.ResponseDto;
import ru.resprojects.dfops.dto.account.AccountDto;
import ru.resprojects.dfops.exception.ResourceNotFoundException;
import ru.resprojects.dfops.model.Account;
import ru.resprojects.dfops.model.Employee;
import ru.resprojects.dfops.service.AccountService;
import ru.resprojects.dfops.service.EmployeeService;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = {"test"})
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
    scripts = {"classpath:schema-h2.sql", "classpath:data-h2.sql"},
    config = @SqlConfig(encoding = "UTF-8"))
public class AccountControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private AccountService accountService;

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void whenRequestGetAllAccountsThenReturnStatus200AndNotEmptyAccountElementList() {
        ParameterizedTypeReference<ResponseDto<Account>> responseType = new ParameterizedTypeReference<>() { };

        ResponseEntity<ResponseDto<Account>> response = restTemplate.exchange("/rest/v1/account/5000", HttpMethod.GET, null, responseType);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getElements()).isNotEmpty();
    }

    @Test
    public void whenRequestGetAllAccountsForEmployeeIdWithoutAccountsThenReturnStatus204() {
        Employee employee = employeeService.create(new Employee("Alex", "alex@example.com"));
        ParameterizedTypeReference<ResponseDto<Account>> responseType = new ParameterizedTypeReference<>() { };

        ResponseEntity<ResponseDto<Account>> response = restTemplate.exchange("/rest/v1/account/" + employee.getId(), HttpMethod.GET, null, responseType);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void whenRequestGetAllAccountsWithIncorrectEmployeeIdThenReturnStatus404() {
        ParameterizedTypeReference<ResponseDto<Account>> responseType = new ParameterizedTypeReference<>() { };

        ResponseEntity<ResponseDto<Account>> response = restTemplate.exchange("/rest/v1/account/1", HttpMethod.GET, null, responseType);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void whenRequestCreateAccountThenReturnStatus200AccountWithId() {
        AccountDto accountDto = new AccountDto(5000L, "1234");

        ResponseEntity<Account> response = restTemplate.postForEntity("/rest/v1/account/create", accountDto, Account.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getId()).isNotNull();
    }

    @Test
    public void whenRequestCreateAccountWithIncorrectEmployeeIdThenReturnStatus404() {
        AccountDto accountDto = new AccountDto(1L, "1234");

        ResponseEntity<Account> response = restTemplate.postForEntity("/rest/v1/account/create", accountDto, Account.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void whenRequestCreateAccountWithIncorrectBankAccountThenReturnStatus400() {
        AccountDto accountDto = new AccountDto(5000L, null);

        ResponseEntity<Account> response = restTemplate.postForEntity("/rest/v1/account/create", accountDto, Account.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void whenRequestCreateAccountWithExistentBankAccountThenReturnStatus409() {
        AccountDto accountDto = new AccountDto(5000L, "4154014152522741");

        ResponseEntity<Account> response = restTemplate.postForEntity("/rest/v1/account/create", accountDto, Account.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
    }

    @Test
    public void whenRequestAccountByIdThenReturnStatus200AndAccount() {
        ResponseEntity<Account> response = restTemplate.getForEntity("/rest/v1/account/get/5002", Account.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    public void whenTryRequestNonexistentAccountThenReturnStatus404() {
        ResponseEntity<Account> response = restTemplate.getForEntity("/rest/v1/account/get/1010", Account.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void whenRequestDeleteAccountByIdThenDeleteAccount() {
        restTemplate.delete("/rest/v1/account/5002");

        accountService.get(5002);
    }

    @Test
    public void whenTryRequestDeleteAccountWithIncorrectIdThenReturnStatus404() {
        ResponseEntity<Void> response = restTemplate.exchange("/rest/v1/account/1010", HttpMethod.DELETE, null, Void.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }


}
