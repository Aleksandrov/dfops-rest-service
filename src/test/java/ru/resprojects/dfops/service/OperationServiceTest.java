package ru.resprojects.dfops.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringRunner;
import ru.resprojects.dfops.DfopsApplication;
import ru.resprojects.dfops.dto.operation.OperationAmountDto;
import ru.resprojects.dfops.exception.BadResourceException;
import ru.resprojects.dfops.model.Account;
import ru.resprojects.dfops.model.Employee;
import ru.resprojects.dfops.model.Operation;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DfopsApplication.class)
@ActiveProfiles(profiles = {"test"})
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
    scripts = {"classpath:schema-h2.sql", "classpath:data-h2.sql"},
    config = @SqlConfig(encoding = "UTF-8"))
public class OperationServiceTest {

    private static final long EMPLOYEE_ONE_ID = 5000L;
    private static final long EMPLOYEE_TWO_ID = 5001L;
    private static final long ACCOUNT_ONE_EMPLOYEE_ONE_ID = 5002L;
    private static final long ACCOUNT_TWO_EMPLOYEE_ONE_ID = 5003L;
    private static final long ACCOUNT_THREE_EMPLOYEE_TWO_ID = 5006L;
    private static final double CURRENT_BALANCE_ACCOUNT_ONE = 1473.47;
    private static final long ALL_OPERATIONS_ACCOUNT_ONE = 5;

    @Autowired
    private OperationService operationService;

    private Account accountOne;
    private Account accountTwo;
    private Account accountThree;

    @Before
    public void init() {
        Employee employeeOne = new Employee(EMPLOYEE_ONE_ID, "Ivanov Ivan Ivanovich", "ivanov@example.com");
        Employee employeeTwo = new Employee(EMPLOYEE_TWO_ID, "Petrov Vasily Victorovich", "petrov@example.com");
        accountOne = new Account(ACCOUNT_ONE_EMPLOYEE_ONE_ID, employeeOne, "4154014152522741");
        accountTwo = new Account(ACCOUNT_TWO_EMPLOYEE_ONE_ID, employeeOne, "4131668358915203");
        accountThree = new Account(ACCOUNT_THREE_EMPLOYEE_TWO_ID, employeeTwo, "4132555843841699");
    }

    @Test
    public void getCurrentBalanceTest() {
        double currentBalance = operationService.getCurrentBalance(ACCOUNT_ONE_EMPLOYEE_ONE_ID).doubleValue();

        assertThat(currentBalance).isEqualTo(CURRENT_BALANCE_ACCOUNT_ONE);
    }

    @Test
    public void whenDepositToAccountThenIncreaseCurrentBalance() {
        double amount = 200;
        double originalBalance = operationService.getCurrentBalance(ACCOUNT_ONE_EMPLOYEE_ONE_ID).doubleValue();

        operationService.deposit(accountOne, amount);

        double changedBalance = operationService.getCurrentBalance(ACCOUNT_ONE_EMPLOYEE_ONE_ID).doubleValue();
        assertThat(changedBalance).isGreaterThan(originalBalance);
        assertThat(changedBalance - originalBalance).isEqualTo(amount);
    }

    @Test
    public void whenWithdrawFromAccountThenDecreaseCurrentBalance() {
        double amount = 200;
        double originalBalance = operationService.getCurrentBalance(ACCOUNT_ONE_EMPLOYEE_ONE_ID).doubleValue();

        operationService.withdraw(accountOne, amount);

        double changedBalance = operationService.getCurrentBalance(ACCOUNT_ONE_EMPLOYEE_ONE_ID).doubleValue();
        assertThat(changedBalance).isLessThan(originalBalance);
        assertThat(originalBalance - changedBalance).isEqualTo(amount);
    }

    @Test(expected = BadResourceException.class)
    public void whenTryWithdrawFromAccountMoreThanCurrentBalanceThenException() {
        double amount = 200;
        double originalBalance = operationService.getCurrentBalance(ACCOUNT_ONE_EMPLOYEE_ONE_ID).doubleValue();

        operationService.withdraw(accountOne, originalBalance + amount);
    }

    @Test
    public void whenGetDebitBetweenThenReturnDebitAtPeriod() {
        double depositAmount = 200.00;
        LocalDateTime start = LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0));
        LocalDateTime end = LocalDateTime.of(LocalDate.now(), LocalTime.of(23, 59));
        operationService.deposit(accountOne, depositAmount);

        double debitAtPeriod = operationService.getDebitBetween(ACCOUNT_ONE_EMPLOYEE_ONE_ID, start, end).doubleValue();

        assertThat(debitAtPeriod).isEqualTo(depositAmount);
    }

    @Test
    public void whenGetDebitBetweenWithNonexistentAccountIdThenReturnZeroValue() {
        LocalDateTime start = LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0));
        LocalDateTime end = LocalDateTime.of(LocalDate.now(), LocalTime.of(23, 59));

        BigDecimal result = operationService.getDebitBetween(123, start, end);

        assertThat(result).isZero();
    }

    @Test
    public void whenGetCreditBetweenThenReturnCreditAtPeriod() {
        double withdrawAmount = 200.00;
        LocalDateTime start = LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0));
        LocalDateTime end = LocalDateTime.of(LocalDate.now(), LocalTime.of(23, 59));
        operationService.withdraw(accountOne, withdrawAmount);

        double creditAtPeriod = operationService.getCreditBetween(ACCOUNT_ONE_EMPLOYEE_ONE_ID, start, end).doubleValue();

        assertThat(creditAtPeriod).isEqualTo(withdrawAmount);
    }

    @Test
    public void whenGetCreditBetweenWithNonexistentAccountIdThenReturnZeroValue() {
        LocalDateTime start = LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0));
        LocalDateTime end = LocalDateTime.of(LocalDate.now(), LocalTime.of(23, 59));

        BigDecimal result = operationService.getCreditBetween(123, start, end);

        assertThat(result).isZero();
    }

    @Test
    public void whenGetAllByAccountIdThenReturnAllOperationsForAccountId() {
        Page<Operation> allOperationsForAccountOne = operationService.getAllByAccountId(ACCOUNT_ONE_EMPLOYEE_ONE_ID, 1, 10);

        assertThat(allOperationsForAccountOne.hasContent()).isTrue();
        assertThat(allOperationsForAccountOne.getContent()).isNotEmpty();
        assertThat(allOperationsForAccountOne.getContent().size()).isEqualTo(ALL_OPERATIONS_ACCOUNT_ONE);
    }

    @Test
    public void whenGetAllByAccountIdWithNonexistentAccountThenReturnEmptyList() {
        Page<Operation> allOperationsForAccountOne = operationService.getAllByAccountId(123, 1, 10);

        assertThat(allOperationsForAccountOne.hasContent()).isFalse();
    }

    @Test
    public void whenGetAllByAccountIdBetweenThenReturnAllOperationsAtPeriodForAccountId() {
        operationService.deposit(accountOne, 10.00);
        operationService.withdraw(accountOne, 10.00);
        LocalDateTime start = LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0));
        LocalDateTime end = LocalDateTime.of(LocalDate.now(), LocalTime.of(23, 59));
        Page<Operation> allOperationsForAccountOne = operationService.getAllByAccountIdBetween(ACCOUNT_ONE_EMPLOYEE_ONE_ID, start, end, 1, 10);

        assertThat(allOperationsForAccountOne.hasContent()).isTrue();
        assertThat(allOperationsForAccountOne.getContent()).isNotEmpty();
        assertThat(allOperationsForAccountOne.getContent().size()).isEqualTo(2);
    }

    @Test
    public void whenGetAllByAccountIdBetweenWithNonexistentAccountThenReturnEmptyList() {
        LocalDateTime start = LocalDateTime.of(LocalDate.of(2020, Month.JANUARY, 1), LocalTime.of(0, 0));
        LocalDateTime end = LocalDateTime.of(LocalDate.of(2020, Month.DECEMBER, 31), LocalTime.of(23, 59));
        Page<Operation> allOperationsForAccountOne = operationService.getAllByAccountIdBetween(123, start, end, 1, 10);

        assertThat(allOperationsForAccountOne.hasContent()).isFalse();
    }

    @Test
    public void whenTransferFromAccountOneToAccountTwoThenWithdrawFromAccountOneAndDepositToAccountTwo() {
        double transferAmount = 1.00;
        double currentBalanceAccountOne = operationService.getCurrentBalance(ACCOUNT_ONE_EMPLOYEE_ONE_ID).doubleValue();
        double currentBalanceAccountTwo = operationService.getCurrentBalance(ACCOUNT_TWO_EMPLOYEE_ONE_ID).doubleValue();

        operationService.transfer(accountOne, accountTwo, transferAmount);

        double currentBalanceAfterTransferAccountOne = operationService.getCurrentBalance(ACCOUNT_ONE_EMPLOYEE_ONE_ID).doubleValue();
        double currentBalanceAfterTransferAccountTwo = operationService.getCurrentBalance(ACCOUNT_TWO_EMPLOYEE_ONE_ID).doubleValue();

        assertThat(currentBalanceAfterTransferAccountOne).isLessThan(currentBalanceAccountOne);
        assertThat(currentBalanceAfterTransferAccountTwo).isGreaterThan(currentBalanceAccountTwo);
        assertThat(currentBalanceAccountOne - currentBalanceAfterTransferAccountOne).isEqualTo(transferAmount);
        assertThat(currentBalanceAfterTransferAccountTwo - currentBalanceAccountTwo).isEqualTo(transferAmount);
    }

    @Test(expected = BadResourceException.class)
    public void whenTransferFromAccountOneToAccountThreeThenException() {
        double transferAmount = 1.00;

        operationService.transfer(accountOne, accountThree, transferAmount);
    }

    @Test(expected = BadResourceException.class)
    public void whenTransferFromAccountOneToAccountTwoButTransferAmountMoreThanCurrentBalanceFromAccountOneThenException() {
        double transferAmount = operationService.getCurrentBalance(accountOne.getId()).doubleValue() + 10.00;

        operationService.transfer(accountOne, accountThree, transferAmount);
    }

    //TODO переделать тест
    @Test
    public void whenGetOperationsAmountBetweenThenReturnListOfOperationsAmount() {
        LocalDateTime start = LocalDateTime.of(LocalDate.of(2020, Month.MAY, 1), LocalTime.of(0, 0));
        LocalDateTime end = LocalDateTime.of(LocalDate.of(2020, Month.MAY, 31), LocalTime.of(23, 59));
        List<OperationAmountDto> result = operationService.getOperationsAmountBetween(ACCOUNT_ONE_EMPLOYEE_ONE_ID, start, end);

        assertThat(result).isNotEmpty();
    }

}
