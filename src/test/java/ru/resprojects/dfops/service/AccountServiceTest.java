package ru.resprojects.dfops.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringRunner;
import ru.resprojects.dfops.DfopsApplication;
import ru.resprojects.dfops.exception.BadResourceException;
import ru.resprojects.dfops.exception.ResourceAlreadyExistsException;
import ru.resprojects.dfops.exception.ResourceNotFoundException;
import ru.resprojects.dfops.model.Account;
import ru.resprojects.dfops.model.Employee;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DfopsApplication.class)
@ActiveProfiles(profiles = {"test"})
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
    scripts = {"classpath:schema-h2.sql", "classpath:data-h2.sql"},
    config = @SqlConfig(encoding = "UTF-8"))
public class AccountServiceTest {

    private static final long EMPLOYEE_ID = 5000L;

    @Autowired
    private AccountService accountService;

    private Account savedAccount;

    @Before
    public void init() {
        Employee employee = new Employee(EMPLOYEE_ID, "Ivanov Ivan Ivanovich", "ivanov@example.com");
        savedAccount = accountService.create(employee, "1234567899876543");
    }

    @Test
    public void whenGetAllAccountsThenReturnNotEmptyList() {
        Page<Account> accounts = accountService.getAll(EMPLOYEE_ID, 1, 10);

        assertThat(accounts.hasContent()).isTrue();
        assertThat(accounts.getContent()).isNotEmpty();
    }

    @Test
    public void whenGetAccountByIdThenReturnAccount() {
        Account account = accountService.get(savedAccount.getId());

        assertThat(account).isNotNull();
        assertThat(account).isEqualTo(savedAccount);
        assertThat(account.getEmployee().getId()).isNotNull();
    }

    @Test
    public void deleteAccountTest() {
        accountService.delete(savedAccount.getId());

        Page<Account> accounts = accountService.getAll(EMPLOYEE_ID, 1, 10);

        assertThat(accounts.hasContent()).isTrue();
        assertThat(accounts.getContent().contains(savedAccount)).isFalse();
    }

    @Test(expected = BadResourceException.class)
    public void whenTrySaveNullThenException() {
        accountService.create(null, null);
    }

    @Test(expected = ResourceAlreadyExistsException.class)
    public void whenSaveAccountWithExistingBankAccountThenException() {
        accountService.create(savedAccount.getEmployee(), savedAccount.getPersonalAccount());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void whenTryDeleteByNonexistentIdThenException() {
        accountService.delete(5);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void whenGetByNonexistentIdThenException() {
        accountService.get(1);
    }

}
