package ru.resprojects.dfops.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringRunner;
import ru.resprojects.dfops.DfopsApplication;
import ru.resprojects.dfops.exception.BadResourceException;
import ru.resprojects.dfops.exception.ResourceAlreadyExistsException;
import ru.resprojects.dfops.exception.ResourceNotFoundException;
import ru.resprojects.dfops.model.Employee;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DfopsApplication.class)
@ActiveProfiles(profiles = {"test"})
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
    scripts = {"classpath:schema-h2.sql", "classpath:data-h2.sql"},
    config = @SqlConfig(encoding = "UTF-8"))
public class EmployeeServiceTest {

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void whenSaveEmployeeThenReturnEmployeeWithId() {
        Employee employee = new Employee("Alex", "example@example.com");

        Employee employeeWithId = employeeService.create(employee);

        assertThat(employeeWithId).isNotNull();
        assertThat(employeeWithId.getId()).isNotNull();
    }

    @Test
    public void whenGetByEmailThenReturnEmploy() {
        Employee employee = new Employee("Alex", "example@example.com");
        employeeService.create(employee);

        Employee existingEmployee = employeeService.getByEmail("example@example.com");

        assertThat(existingEmployee).isNotNull();
    }

    @Test
    public void deleteEmployeeTest() {
        Employee employee = new Employee("Alex", "example@example.com");
        Employee savedEmployee = employeeService.create(employee);

        employeeService.delete(savedEmployee.getId());

        Page<Employee> employees = employeeService.getAll(1, 10);
        assertThat(employees.hasContent()).isTrue();
        assertThat(employees.getContent().contains(savedEmployee)).isFalse();
    }

    @Test
    public void whenGetAllEmployeesThenReturnNotEmptyListOfEmployees() {
        Page<Employee> employees = employeeService.getAll(1, 10);

        assertThat(employees.getContent()).isNotEmpty();
    }

    @Test(expected = BadResourceException.class)
    public void whenTrySaveNullThenException() {
        employeeService.create(null);
    }

    @Test(expected = ResourceAlreadyExistsException.class)
    public void whenSaveEmployeeWithExistingEmailThenException() {
        Employee employee = new Employee("Alex", "example@example.com");
        employeeService.create(employee);

        employeeService.create(employee);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void whenTryDeleteByNonexistentIdThenException() {
        employeeService.delete(5);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void whenGetByNonexistentIdThenException() {
        employeeService.get(1);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void whenGetByNonexistentEmailThenException() {
        employeeService.getByEmail("abc@example.com");
    }

}
