#!/bin/bash

EXECUTABLE_FILE=dfops.jar
PROPERTIES_FILE=application-prod.properties

HELP="Usage: dfops_linux [KEY]

Script without key is run program in DEFAULT mode.

DEFAULT mode uses postgresql with next parameters:

    url: jdbc:postgresql://localhost/test
    username: test
    password: test

Also available next switches:

--prod - running program in PRODUCTION mode. For running in this mode needed additional
file application-prod.properties with PostgreSQL dataset information.

--demo - running program in DEMO mode. In this mode uses H2 database (in-memory) instead postgresql.

--help - display this is message

Examples:

dfops_linux - run program in DEFAULT mode

dfops_linux --demo - run program in DEMO mode.

dfops_linux --prod - run program in PRODUCTION mode.
"

PROPERTIES_FILE_NOT_FOUND="
WARNING!

You try run program in PRODUCTION mode. For this mode need PostgreSQL but file 
$PROPERTIES_FILE with dataset information is not found. Please fill next information and run program again!

"

if [ -f "$EXECUTABLE_FILE" ]; then
    if [ -z "$1" ]; then
        echo "Running program in DEFAULT mode"
        java -jar "$EXECUTABLE_FILE"
    else
        case "$1" in
            --help)
                echo "$HELP"
            ;;
            --demo)
                echo "Running program in DEMO mode with H2 DB (in-memory)"
                java -jar "$EXECUTABLE_FILE" --spring.profiles.active=demo
            ;;
            --prod)
                if [ -f "$PROPERTIES_FILE" ]; then
                  echo "Running program in PRODUCTION mode with PostgreSQL DB"
                  java -jar "$EXECUTABLE_FILE" --spring.profiles.active=prod
                else
                    echo "$PROPERTIES_FILE_NOT_FOUND"
                    printf 'PostgreSQL database host name or IP address (default localhost): '
                    read -r DFOPS_PGSQL_DB_HOST
                    if [ -z "$DFOPS_PGSQL_DB_HOST" ]; then
                        DFOPS_PGSQL_DB_HOST="jdbc:postgresql://localhost"
                    else
                        DFOPS_PGSQL_DB_HOST="jdbc:postgresql://$DFOPS_PGSQL_DB_HOST"
                    fi
                    printf 'PostgreSQL database port (default 5432): '
                    read -r DFOPS_PGSQL_DB_PORT
                    if [ -z "$DFOPS_PGSQL_DB_PORT" ]; then
                        DFOPS_PGSQL_DB_PORT=5432
                    fi
                    printf 'PostgreSQL database name (default test): '
                    read -r DFOPS_PGSQL_DB_NAME
                    if [ -z "$DFOPS_PGSQL_DB_NAME" ]; then
                        DFOPS_PGSQL_DB_NAME="test"
                    fi
                    printf 'PostgreSQL database user name: '
                    read -r DFOPS_PGSQL_DB_USER
                    printf 'PostgreSQL database password: '
                    read  -r -s DFOPS_PGSQL_DB_PASSWORD
                    echo
                    touch "$PROPERTIES_FILE"
                    {
                      echo "DFOPS_PGSQL_DB_HOST=$DFOPS_PGSQL_DB_HOST"
                      echo "DFOPS_PGSQL_DB_PORT=$DFOPS_PGSQL_DB_PORT"
                      echo "DFOPS_PGSQL_DB_NAME=$DFOPS_PGSQL_DB_NAME"
                      echo "DFOPS_PGSQL_DB_USER=$DFOPS_PGSQL_DB_USER"
                      echo "DFOPS_PGSQL_DB_PASSWORD=$DFOPS_PGSQL_DB_PASSWORD"
                    } > "$PROPERTIES_FILE"
                fi
            ;;
            *)
                echo "dfops_linux: unknown option $1"
                echo "Try 'dfops_linux --help' for more information."
            ;;
        esac
    fi
else
    echo "Executable file dfops.jar is not found!"
fi