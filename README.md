# Задание

Напишите Spring-приложение, предоставляющее REST-сервис для приема и регистрации операций в базе данных по расчетам с водителями. У каждого водителя может быть несколько лицевых счетов.

## Требования

СУБД PostgreSQL 10; создать структуру БД самостоятельно.
Доступ к данным реализовать с помощью Hibernate.

Реализовать операции REST-сервиса; прием параметров и возврат данных – в JSON:
* начисление на счет водителя, 
* списание со счета водителя, 
* получение текущего баланса по счету, 
* перевод между собственными лицевыми счетами водителя,
* получение оборота за период по отдельному лицевому счету (дебет, кредит отдельно),
* получение подробного списка операций за период.

Для объемных операций (например, получение списка операций) реализовать постраничную выдачу.

## Дополнительные требования

* Создать Gradle-проект.
* Хранение исходников – в GIT (например, на gitlab.com).
* Дополнительным плюсом будет реализации unit-тестов.

# Комментарий к выполненной работе

Программу можно запустить в нескольких режимах используя профили spring boot

1. **DEFAULT** - в данном режиме используется база данных postgresql с настройками по умолчанию, а именно `url:jdbc:postgresql://localhost/test, username: test, password: test`. Для запуска используем следующие параметры `java -jar dfops.jar`
1. **DEMO** - в данном режиме используется база данных H2 DB. Для запуска используем следующие параметры `java -jar dfops.jar --spring.profiles.active=demo`
1. **PRODUCTION** - в данном режиме используется база данных postgresql c альтернативными настройками прописанными в файле `application-prod.properties`, данный файл должен находиться в том же каталоге где и запускаемый jar-файл программы. Для запуска используем следующие параметры `java -jar dfops.jar --spring.profiles.active=prod`

Пример содержимого файла `application-prod.properties`

```
DFOPS_PGSQL_DB_HOST=jdbc:postgresql://localhost
DFOPS_PGSQL_DB_PORT=5432
DFOPS_PGSQL_DB_NAME=test
DFOPS_PGSQL_DB_USER=test
DFOPS_PGSQL_DB_PASSWORD=test
```

Так же для запуска программы в linux, можно воспользоваться скриптом `dfops_linux.sh` , при этом запускаемый jar-файл должен называться `dfops.jar` и находиться в том же каталоге, где и скрипт. Выполните `dfops_linux.sh --help` для получения помощи. При запуске в режиме `PRODUCTION` будет выполнена проверка на наличие файла `application-prod.properties`, если он не найден, то запустится интерактивный режим, где будет предложено заполнить необходимые данные.

## Работа с программой

### Инициализация БД

Если работа ведётся с postgresql можно воспользуйтесь файлами `ddl-postgresql.sql`, `schema-postgresql.sql`, `data-postgresql.sql` для инициализации БД см. каталог `init_postgresql_db`

### Прочее

1. Точка входа `http://localhost:8080/rest/v1/`
1. Документацию по API использует OpenApi c ui. Для доступа к ui используем адрес `http://localhost:8080/rest/v1/swagger-ui/index.html?configUrl=/rest/v1/v3/api-docs/swagger-config` , в представлении json используем `http://localhost:8080/rest/v1/v3/api-docs`
1. В каталоге `docker` при наличии docker и docker compose можно запустить docker-образ с postgresql версии 10.
1. В gitlab настроен CI/CD. Есть возможность скачать последнюю версию сборки по ссылке https://gitlab.com/Aleksandrov/dfops-rest-service/-/pipelines    

---

Тестовое задание выполнил

Александров А.А. (alexandrov@resprojects.ru)

ссылка на профиль hh.ru - https://hh.ru/resume/7cdada75ff015e78530039ed1f366c4b4a5273